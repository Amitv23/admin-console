# ZipR Device Admin Console

## Pre-requisites and Preparation

1. Install [Docker Desktop](https://www.docker.com/products/docker-desktop)
2. Create a [Send Grid](https://sendgrid.com/) account (This is needed for mailing the magic login link) 
    2.1. After logging in to Send Grid, go to *"Email API --> Integration Guide"*
    2.2. Click *"Choose"* under  *"SMTP Relay"*
    2.3. Type an API key name of choice ("my-key") and click *"Create"*
    2.4. Now copy the password string shown and keep it aside
    2.5. Under *"Settings"* in the left panel, click on *"Sender Authentication"*
    2.6. Choose "Single Sender Authentication" and complete the steps to authenticate your email address
3. Create a file named `.env.development` under this directory with the following contents:
```
EMAIL_SERVER="smtps://apikey:<the password you noted in step 2.4>@smtp.sendgrid.net:465"
EMAIL_FROM="<the email address you authenticated in step 2.6>"
DATABASE_URL="mongodb://admin:password@127.0.0.1:27017/nextdb?authSource=admin"
SECRET="<any random string of characters of atleast 64 characters>"
NEXTAUTH_URL="http://localhost:3000"
MONGODB_URI="mongodb://admin:password@127.0.0.1:27017/nextdb?authSource=admin"
```

## Development Flow

1.  Build the development image
```
    docker build -t dev-adm-cnsl-dev -f Dockerfile.dev .
```
2. Run the `mongodb` instance
```
    docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password mongo  
```
3. Run the development image
```
    docker run -p 3000:3000 -v node_modules -v .next -v $PWD:/app -it dev-adm-cnsl-dev 
```
4. You should be able to see the admin console at [http://localhost:3000](http://localhost:3000)
5. Any changes you make under `pages/`, `src/` or `lib/` should be instantly visible after an automatic reload.

## In case of windows:

1. Download Docker Desktop Installer for Windows(https://hub.docker.com/editions/community/docker-ce-desktop-windows/).
2. Install any version of Ubuntu(preferably Ubuntu 20.04 LTS) from microsoft store.
3. After installing Ubuntu, Create username and password.
4. Open the project in VS code and install Remote - WSL extension. In the terminal, enter the password (the one set by you in step 3).
5. Now you can refer to the instructions given above .i.e., # Development Flow (in line number 23). 
```